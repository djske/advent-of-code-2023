from pathlib import Path

from more_itertools import chunked


def parse_input(infile: Path) -> str:
    with infile.open() as f:
        return f.read().strip()


def move(current: tuple[int, int], char: str) -> tuple[int, int]:
    match char:
        case "^":
            current = current[0] - 1, current[1]
        case ">":
            current = current[0], current[1] + 1
        case "<":
            current = current[0], current[1] - 1
        case "v":
            current = current[0] + 1, current[1]

    return current


def part_one(infile: Path) -> int:
    current = (
        0,
        0,
    )
    seen = {current}

    for char in parse_input(infile):
        current = move(current, char)
        seen.add(current)

    return len(seen)


def part_two(infile: Path) -> int:
    santa_current = (0, 0)
    robo_santa_current = (0, 0)
    seen = {santa_current}

    for santa_char, robo_char in chunked(parse_input(infile), 2):
        santa_current = move(santa_current, santa_char)
        seen.add(santa_current)

        robo_santa_current = move(robo_santa_current, robo_char)
        seen.add(robo_santa_current)

    return len(seen)
