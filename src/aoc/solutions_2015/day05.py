from collections import defaultdict
from pathlib import Path


def parse_input(infile: Path) -> list[str]:
    with infile.open() as f:
        return [line.strip().upper() for line in f]


VOWELS = frozenset("AEIOU")
FORBIDDEN = frozenset(
    (
        (
            "A",
            "B",
        ),
        (
            "C",
            "D",
        ),
        (
            "P",
            "Q",
        ),
        (
            "X",
            "Y",
        ),
    )
)


def is_nice_part_one(string: str) -> bool:
    previous = "N/A"
    max_vowel_count = 2

    double_letter = False
    forbidden_str = False
    vowel_count = 0

    for char in string:
        if char == previous:
            double_letter = True

        if char in VOWELS:
            vowel_count += 1

        if (previous, char) in FORBIDDEN:
            forbidden_str = True

        previous = char

    return double_letter and vowel_count > max_vowel_count and not forbidden_str


def is_nice_part_two(string: str) -> bool:
    pair_dct: dict[tuple[str, str], set[int]] = defaultdict(set)
    single_dct: dict[str, set[int]] = defaultdict(set)

    non_overlapping_double = False
    close_repeat = False

    previous = "N/A"

    for idx, char in enumerate(string):
        if idx - 2 in single_dct[char]:
            close_repeat = True
        single_dct[char].add(idx)

        pair = (previous, char)
        if pair_dct[pair] - {idx - 1}:
            non_overlapping_double = True
        pair_dct[pair].add(idx)

        previous = char

    return non_overlapping_double and close_repeat


def part_one(infile: Path) -> int:
    data = parse_input(infile)
    return sum(is_nice_part_one(string) for string in data)


def part_two(infile: Path) -> int:
    data = parse_input(infile)
    return sum(is_nice_part_two(string) for string in data)
