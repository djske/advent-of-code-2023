from pathlib import Path

DELTAS = {"(": 1, ")": -1}


def part_one(infile: Path) -> int:
    with infile.open() as f:
        return sum(DELTAS.get(char, 0) for char in f.read())


def part_two(infile: Path) -> int:
    total = 0
    with infile.open() as f:
        for position, char in enumerate(f.read().strip(), 1):
            total += DELTAS[char]
            if total == -1:
                return position

    err = "Unexpectedly finished without visiting -1"
    raise AssertionError(err)
