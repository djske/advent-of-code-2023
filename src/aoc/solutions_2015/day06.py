import re
from collections.abc import Iterator
from enum import Enum
from pathlib import Path

import numpy as np

parser = re.compile(r"(?P<inst>(turn off|turn on|toggle)) (?P<i1>\d+),(?P<j1>\d+) through (?P<i2>\d+),(?P<j2>\d+)")


class Instruction(Enum):
    TURN_OFF = "turn off"
    TURN_ON = "turn on"
    TOGGLE = "toggle"


Coords = tuple[int, int]


def parse_input(infile: Path) -> Iterator[tuple[Instruction, Coords, Coords]]:
    with infile.open() as f:
        for line in f:
            match = parser.match(line)
            if not match:
                err = "match should not be None"
                raise AssertionError(err)
            groups = match.groupdict()

            instruction = Instruction(groups["inst"])
            start = int(groups["i1"]), int(groups["j1"])
            end = int(groups["i2"]), int(groups["j2"])

            yield instruction, start, end


def part_one(infile: Path) -> int:
    grid = np.full((1000, 1000), -1, dtype=np.int8)
    for inst, (i1, j1), (i2, j2) in parse_input(infile):
        match inst:
            case Instruction.TURN_ON:
                grid[i1 : i2 + 1, j1 : j2 + 1] = 1
            case Instruction.TURN_OFF:
                grid[i1 : i2 + 1, j1 : j2 + 1] = -1
            case Instruction.TOGGLE:
                grid[i1 : i2 + 1, j1 : j2 + 1] *= -1

    return len(np.where(grid == 1)[0])


def part_two(infile: Path) -> int:
    grid = np.zeros((1000, 1000), dtype=np.int64)

    for inst, (i1, j1), (i2, j2) in parse_input(infile):
        match inst:
            case Instruction.TURN_ON:
                grid[i1 : i2 + 1, j1 : j2 + 1] += 1
            case Instruction.TURN_OFF:
                grid[i1 : i2 + 1, j1 : j2 + 1] -= 1
                grid[np.where(grid < 0)] = 0
            case Instruction.TOGGLE:
                grid[i1 : i2 + 1, j1 : j2 + 1] += 2

    return int(grid.sum())
