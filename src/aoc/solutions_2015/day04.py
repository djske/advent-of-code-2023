from hashlib import md5
from pathlib import Path


def parse_input(infile: Path) -> str:
    return infile.read_text().strip()


def find_n_with_k_leading_zeros(key: str, k: int) -> int:
    counter = 0
    prefix = "0" * k

    while True:
        counter += 1
        result = md5(f"{key}{counter}".encode()).hexdigest()  # noqa: S324
        if result.startswith(prefix):
            break

    return counter


def part_one(infile: Path) -> int:
    key = parse_input(infile)
    return find_n_with_k_leading_zeros(key, 5)


def part_two(infile: Path) -> int:
    key = parse_input(infile)
    return find_n_with_k_leading_zeros(key, 6)
