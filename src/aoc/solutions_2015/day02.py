from pathlib import Path


def parse_input(infile: Path) -> list[list[int]]:
    with infile.open() as f:
        return [sorted(int(i) for i in line.strip().split("x")) for line in f]


def part_one(infile: Path) -> int:
    total = 0
    for d1, d2, d3 in parse_input(infile):
        total += d1 * d2 * 3 + d1 * d3 * 2 + d2 * d3 * 2
    return total


def part_two(infile: Path) -> int:
    total = 0
    for d1, d2, d3 in parse_input(infile):
        total += 2 * d1 + 2 * d2 + d1 * d2 * d3
    return total
