import importlib
from pathlib import Path

import click


@click.command()
@click.option("--day", required=True, type=int)
@click.option("--part", required=True, type=click.Choice(["1", "2"]))
@click.option("--infile", required=True, type=click.Path(exists=True))
@click.option("--year", required=True, type=int)
def run(day: int, part: str, infile: str | Path, year: int) -> None:
    module = importlib.import_module(f"aoc.solutions_{year}.day{str(day).zfill(2)}")
    infile = Path(infile)
    if part == "1":
        click.echo(module.part_one(infile))
    else:
        click.echo(module.part_two(infile))


if __name__ == "__main__":
    run()
