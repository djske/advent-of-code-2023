from collections import deque
from pathlib import Path

import networkx as nx  # type: ignore[import-untyped]
from more_itertools import windowed


def parse_input(infile: Path) -> nx.DiGraph:
    data = infile.read_text().strip().split("\n")
    i_shape = len(data)
    j_shape = len(data[0])

    deltas = {
        ".": (
            (-1, 0),
            (1, 0),
            (0, -1),
            (0, 1),
        ),
        ">": ((0, 1),),
        "<": ((0, -1),),
        "^": ((-1, 0),),
        "v": ((1, 0),),
        "#": (),
    }

    graph = nx.DiGraph()
    queue: deque[tuple[int, int]] = deque()

    start = 0, data[0].index(".")
    queue.appendleft(start)

    while queue:
        i, j = queue.pop()
        for di, dj in deltas[data[i][j]]:
            new_i, new_j = i + di, j + dj
            # Don't backtrack.
            if (
                (new_i, new_j),
                (i, j),
            ) in graph.out_edges((new_i, new_j)):
                continue
            # Don't attempt to move off grid.
            if not (0 <= new_i < i_shape and 0 <= new_j < j_shape):
                continue
            # Don't move into the forest.
            if data[new_i][new_j] == "#":
                continue
            # Don't step onto a slope from the diretion you'd come off it.
            if data[new_i][new_j] in "><v^":
                next_di, next_dj = deltas[data[new_i][new_j]][0]
                if (new_i + next_di, new_j + next_dj) == (i, j):
                    continue
            if (new_i, new_j) not in graph:
                queue.appendleft((new_i, new_j))
            graph.add_edge((i, j), (new_i, new_j))

    return graph


def reduce_graph(graph: nx.Graph) -> nx.Graph:
    new_graph = graph.copy()

    for node_a in list(new_graph.nodes):
        if node_a not in new_graph:
            continue
        neighbours_a = list(new_graph.neighbors(node_a))
        # For each of a's neighbours
        for node_b in neighbours_a:
            neighbours_b = list(new_graph.neighbors(node_b))
            if len(neighbours_b) != 2:  # noqa: PLR2004
                continue

            neighbours_b.remove(node_a)
            node_c = neighbours_b.pop()

            try:
                new_weight = graph.edges[(node_a, node_b)].get("weight", 1) + graph.edges[(node_b, node_c)].get(
                    "weight", 1
                )

                new_graph.remove_node(node_b)
                new_graph.add_edge(node_a, node_c, weight=new_weight)
            except KeyError:
                continue

    if new_graph.edges != graph.edges or new_graph.nodes != graph.nodes:
        return reduce_graph(new_graph)
    return graph


def part_one(infile: Path) -> int:
    graph = parse_input(infile)
    path = nx.dag_longest_path(graph)
    return len(path) - 1


def part_two(infile: Path) -> int:
    graph = parse_input(infile)
    graph = graph.to_undirected()
    graph = reduce_graph(graph)

    highest = 0
    for path in nx.all_simple_paths(graph, min(graph.nodes), max(graph.nodes)):
        total = sum(graph.edges[e]["weight"] for e in windowed(path, 2))
        if total > highest:
            highest = total

    return highest
