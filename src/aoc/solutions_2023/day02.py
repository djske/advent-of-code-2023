import re
from dataclasses import dataclass
from pathlib import Path
from typing import Iterator

BLUE_RE = re.compile(r"(?P<blue>\d+) blue")
RED_RE = re.compile(r"(?P<red>\d+) red")
GREEN_RE = re.compile(r"(?P<green>\d+) green")
GAME_RE = re.compile(r"Game (?P<game>\d+)")

COLOUR_RE_MAP = {"green": GREEN_RE, "red": RED_RE, "blue": BLUE_RE}


def iter_input(infile: Path) -> Iterator[str]:
    with infile.open() as f:
        for line in f:
            if stripped_line := line.strip():
                yield stripped_line


@dataclass
class Game:
    game_id: int
    highest_green: int
    highest_red: int
    highest_blue: int

    @classmethod
    def from_string(cls, line: str) -> "Game":
        kwargs = {}

        game_id_search = GAME_RE.search(line)
        if not game_id_search:
            err = f"Could not detect Game ID in string {line!r}"
            raise ValueError(err)
        kwargs["game_id"] = int(game_id_search.groupdict()["game"])

        for colour, regex in COLOUR_RE_MAP.items():
            kwargs[f"highest_{colour}"] = max([int(match.groupdict()[colour]) for match in regex.finditer(line)])

        return cls(**kwargs)


def part_one(input_file: Path) -> int:
    max_red = 12
    max_green = 13
    max_blue = 14

    game_id_total = 0

    for line in iter_input(input_file):
        game = Game.from_string(line)

        if game.highest_red > max_red:
            continue
        elif game.highest_green > max_green:
            continue
        elif game.highest_blue > max_blue:
            continue
        else:
            game_id_total += game.game_id

    return game_id_total


def part_two(input_file: Path) -> int:
    power_total = 0

    for line in iter_input(input_file):
        game = Game.from_string(line)

        power_total += game.highest_blue * game.highest_green * game.highest_red

    return power_total
