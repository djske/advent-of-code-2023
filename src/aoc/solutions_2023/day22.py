from collections.abc import Callable
from copy import deepcopy
from pathlib import Path
from typing import Final, Literal

import numpy as np
import numpy.typing as npt

Coord = tuple[int, int, int]
COORD_DIMS: Final[Literal[3]] = 3


def parse_coord(string: str) -> tuple[int, int, int]:
    coord = tuple(int(i) for i in string.split(","))
    if not len(coord) == COORD_DIMS:
        raise AssertionError()
    return coord


def parse_input(infile: Path) -> list[tuple[Coord, Coord]]:
    coord_pairs = []
    with infile.open() as f:
        for line in f:
            stripped = line.strip()
            a, b = (parse_coord(coord) for coord in stripped.split("~"))
            coord_pair = a, b
            coord_pairs.append(coord_pair)

    return coord_pairs


def create_grid(coord_pairs: list[tuple[Coord, Coord]]) -> npt.NDArray[np.int32]:
    max_x = 0
    max_y = 0
    max_z = 0

    for (x1, y1, z1), (x2, y2, z2) in coord_pairs:
        max_x = max(max_x, x1 + 1, x2 + 1)
        max_y = max(max_y, y1 + 1, y2 + 1)
        max_z = max(max_z, z1 + 1, z2 + 1)

    grid = np.zeros((max_x, max_y, max_z), dtype=np.int32)

    for idx, ((x1, y1, z1), (x2, y2, z2)) in enumerate(coord_pairs, 1):
        grid[x1 : x2 + 1, y1 : y2 + 1, z1 : z2 + 1] = idx

    return grid


def apply_gravity(grid: npt.NDArray[np.int32], callback: Callable[[int, set[int]], None]) -> None:
    moved: set[np.int32] = set()

    for k in range(grid.shape[2]):
        layer = grid[:, :, k]
        bricks = set(layer[np.where(layer != 0)])

        for brick in bricks - moved:
            brick_pos = np.where(grid == brick)
            while True:
                if not all(z - 1 != -1 for *_, z in zip(*brick_pos)):
                    break

                bricks_on = set(grid[(brick_pos[0], brick_pos[1], brick_pos[2] - 1)])
                bricks_on -= {brick, 0.0}

                if bricks_on:
                    callback(brick, bricks_on)
                    break

                grid[brick_pos] = 0
                k_arr = brick_pos[2]
                k_arr -= 1
                grid[brick_pos] = brick

            moved.add(brick)


def part_one(infile: Path) -> int:
    coords = parse_input(infile)
    grid = create_grid(coords)

    all_bricks = set(range(1, len(coords) + 1))

    def callback(_: int, bricks_on: set[int]) -> None:
        nonlocal all_bricks
        if len(bricks_on) == 1:
            all_bricks -= bricks_on

    apply_gravity(grid, callback)

    return len(all_bricks)


def part_two(infile: Path) -> int:
    coords = parse_input(infile)
    grid = create_grid(coords)

    resting_dependencies: dict[int, set[int]] = {brick: set() for brick, _ in enumerate(coords, 1)}

    def callback(brick: int, bricks_on: set[int]) -> None:
        nonlocal resting_dependencies
        resting_dependencies[brick] = bricks_on

    apply_gravity(grid, callback)

    total = 0
    for i in resting_dependencies:
        cp_dependencies = deepcopy(resting_dependencies)
        deleted = {i}
        del cp_dependencies[i]

        while True:
            to_delete = []
            for key, value in cp_dependencies.items():
                # If was supported and now all supports have been removed
                if value and not value - deleted:
                    to_delete.append(key)
                    deleted.add(key)

            if not to_delete:
                break
            cp_dependencies = {key: value for key, value in cp_dependencies.items() if key not in to_delete}

        total += len(deleted) - 1

    return total
