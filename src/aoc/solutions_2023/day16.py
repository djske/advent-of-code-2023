from collections import deque
from collections.abc import Callable
from dataclasses import dataclass
from enum import Enum
from functools import cache
from pathlib import Path

import numpy as np
import numpy.typing as npt


class Direction(Enum):
    NORTH = (-1, 0)
    SOUTH = (1, 0)
    EAST = (0, 1)
    WEST = (0, -1)


@dataclass(frozen=True)
class State:
    position: tuple[int, int]
    direction: Direction

    def next_square(self) -> tuple[int, int]:
        return (
            self.position[0] + self.direction.value[0],
            self.position[1] + self.direction.value[1],
        )


MIRROR_CHANGES = {
    92: {
        Direction.NORTH: Direction.WEST,
        Direction.WEST: Direction.NORTH,
        Direction.EAST: Direction.SOUTH,
        Direction.SOUTH: Direction.EAST,
    },
    47: {
        Direction.EAST: Direction.NORTH,
        Direction.NORTH: Direction.EAST,
        Direction.SOUTH: Direction.WEST,
        Direction.WEST: Direction.SOUTH,
    },
}


def parse_input(infile: Path) -> npt.NDArray[np.int32]:
    data: list[list[int]] = []
    with infile.open("rb") as f:
        for line in f:
            data.append(list(line.strip()))

    return np.array(data, dtype=np.int32)


def get_n_energised_squares(start: State, get_next_states_callback: Callable[[State], list[State]]) -> int:
    queue = deque([start])
    seen_states = set()

    while queue:
        current = queue.pop()

        for next_state in get_next_states_callback(current):
            if next_state not in seen_states:
                seen_states.add(next_state)
                queue.appendleft(next_state)

    energised_positions = {i.position for i in seen_states}
    return len(energised_positions)


def create_get_next_states_callback(grid: npt.NDArray[np.int32]) -> Callable[[State], list[State]]:
    @cache
    def get_next_states(state: State) -> list[State]:
        nonlocal grid

        # Calculate next square + bounds check
        next_square = state.next_square()
        if not 0 <= next_square[0] < grid.shape[0]:
            return []
        if not 0 <= next_square[1] < grid.shape[1]:
            return []

        # Get next states based on the direction.
        match (grid[next_square], state.direction):
            case (124, Direction.EAST | Direction.WEST):
                next_directions = [Direction.NORTH, Direction.SOUTH]
            case (45, Direction.NORTH | Direction.SOUTH):
                next_directions = [Direction.EAST, Direction.WEST]
            case (47 | 92, direction):
                next_directions = [MIRROR_CHANGES[grid[next_square]][direction]]
            case _:
                next_directions = [state.direction]

        return [State(next_square, next_direction) for next_direction in next_directions]

    return get_next_states


def part_one(infile: Path) -> int:
    data = parse_input(infile)
    callback = create_get_next_states_callback(data)
    start = State(
        (
            0,
            -1,
        ),
        Direction.EAST,
    )
    return get_n_energised_squares(start, callback)


def part_two(infile: Path) -> int:
    data = parse_input(infile)
    results = []

    callback = create_get_next_states_callback(data)

    for row in range(data.shape[0]):
        results.append(get_n_energised_squares(State((row, -1), Direction.EAST), callback))
        results.append(get_n_energised_squares(State((row, data.shape[1]), Direction.WEST), callback))

    for col in range(data.shape[1]):
        results.append(get_n_energised_squares(State((-1, col), Direction.SOUTH), callback))
        results.append(get_n_energised_squares(State((data.shape[0], col), Direction.NORTH), callback))

    return max(results)
