from collections.abc import Sequence
from dataclasses import dataclass
from enum import Enum
from itertools import chain
from pathlib import Path

import networkx as nx  # type: ignore[import-untyped]
import numpy as np

Grid = Sequence[Sequence["Tile"]]
Coords = tuple[int, int]


class TileType(Enum):
    NORTH_SOUTH = "|"
    NORTH_EAST = "L"
    NORTH_WEST = "J"
    EAST_SOUTH = "F"
    EAST_WEST = "-"
    SOUTH_WEST = "7"
    START = "S"
    NULL = "."

    def get_direction_deltas(self) -> tuple[tuple[int, int], ...]:
        if self == TileType.NORTH_SOUTH:
            return (-1, 0), (1, 0)
        if self == TileType.NORTH_EAST:
            return (-1, 0), (0, 1)
        if self == TileType.NORTH_WEST:
            return (-1, 0), (0, -1)
        if self == TileType.EAST_SOUTH:
            return (0, 1), (1, 0)
        if self == TileType.EAST_WEST:
            return (0, 1), (0, -1)
        if self == TileType.SOUTH_WEST:
            return (1, 0), (0, -1)
        if self == TileType.START:
            return (1, 0), (-1, 0), (0, 1), (0, -1)
        if self == TileType.NULL:
            return ()


@dataclass
class Tile:
    i: int
    j: int
    type_: TileType

    def __repr__(self) -> str:
        return self.type_.value

    @property
    def coords(self) -> Coords:
        return (self.i, self.j)

    @property
    def adjacent_pipes(self) -> list[Coords]:
        deltas = self.type_.get_direction_deltas()
        return [(self.i + i_delta, self.j + j_delta) for i_delta, j_delta in deltas]


def parse_infile(infile: Path) -> list[list[Tile]]:
    with infile.open() as f:
        return [
            [Tile(i=i_idx, j=j_idx, type_=TileType(tile)) for j_idx, tile in enumerate(line.strip())]
            for i_idx, line in enumerate(f)
        ]


def get_undirected_graph_of_largest_cycle_involving_start(grid: Grid) -> nx.Graph:
    network = nx.DiGraph()

    start = (-1, -1)

    # - Find out the coordinates of the start tile.
    # - Draw an edge, U -> V, if tile U has connections pointing at tile V.
    for source_tile in chain(*grid):
        if source_tile.type_ == TileType.START:
            start = source_tile.coords

        for target_tile in source_tile.adjacent_pipes:
            network.add_edge(source_tile.coords, target_tile)

    # Filer out edges so that we only keep edges where two tiles connect to
    # one another (i.e., we have an edge U -> V and V -> U)
    for node in network:
        incoming = {i[0] for i in network.in_edges(node)}
        outgoing = {i[1] for i in network.out_edges(node)}

        # Remove incoming only
        for i in incoming - outgoing:
            network.remove_edge(i, node)
        # Remove outgoing only
        for j in outgoing - incoming:
            network.remove_edge(node, j)

    # We can use cycle_basis because all nodes (except the start in this model)
    # can be part of one cycle at most. Thus, the items returned will be the
    # possible cycles involving start.
    undirected_network = network.to_undirected()

    cycle_nodes = max(
        (cycle_nodes for cycle_nodes in nx.cycle_basis(undirected_network) if start in set(cycle_nodes)),
        key=lambda i: len(i),
    )
    return undirected_network.subgraph(cycle_nodes)


def part_one(infile: Path) -> int:
    data = parse_infile(infile)
    return len(get_undirected_graph_of_largest_cycle_involving_start(data)) // 2


def part_two(infile: Path) -> int:
    data = parse_infile(infile)

    cycle = get_undirected_graph_of_largest_cycle_involving_start(data)

    for tile in chain(*data):
        # Set pipes not part of the main loop to Tile.NULL.
        if tile.coords not in cycle:
            tile.type_ = TileType.NULL

        # Determine the type of the start tile.
        elif tile.type_ == TileType.START:
            neighbors = list(cycle.neighbors(tile.coords))
            deltas = [(i - tile.i, j - tile.j) for i, j in neighbors]
            deltas.sort()

            direction = [tile for tile in TileType if sorted(tile.get_direction_deltas()) == deltas]
            tile.type_ = direction.pop()

    # Solve using the ray casting algorithm. We use diagonal lines as this
    # allows simpler reasoning.
    grid_arr = np.array(data)
    diagonal_idx = -len(data) + 1
    in_counter = 0

    while diagonal := list(grid_arr.diagonal(diagonal_idx, 0, 1)):
        borders_crossed = 0
        for tile in diagonal:
            if borders_crossed % 2 == 1 and tile.type_ == TileType.NULL:
                in_counter += 1
            if tile.type_ in {TileType.EAST_WEST, TileType.NORTH_SOUTH, TileType.EAST_SOUTH, TileType.NORTH_WEST}:
                borders_crossed += 1
        diagonal_idx += 1

    return in_counter
