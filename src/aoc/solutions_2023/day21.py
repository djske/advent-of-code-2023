from collections import defaultdict, deque
from collections.abc import Iterator
from pathlib import Path

import numpy as np
import numpy.typing as npt

Coord = tuple[int, int]
Tile = tuple[int, int]

EMPTY = 0
START = 1
ROCK = 2


def parse_input(infile: Path) -> npt.NDArray[np.int8]:
    char_map = {"S": START, ".": EMPTY, "#": ROCK}
    grid = []
    with infile.open() as f:
        for line in f:
            grid.append([char_map[i] for i in line.strip()])
    return np.array(grid, dtype=np.int32)


def visit_neighbours(
    current: Coord,
    grid: npt.NDArray[np.int8],
    even_visited: set[Coord],
    odd_visited: set[Coord],
    steps_taken: int,
    max_steps: int,
) -> Iterator[Coord]:
    if steps_taken == max_steps:
        return

    deltas = (
        (-1, 0),
        (1, 0),
        (0, -1),
        (0, 1),
    )
    steps_taken += 1
    for di, dj in deltas:
        i, j = current[0] + di, current[1] + dj
        if (i, j) in even_visited | odd_visited:
            continue
        if not 0 <= i < grid.shape[0]:
            continue
        if not 0 <= j < grid.shape[1]:
            continue
        if grid[i, j] != 0:
            continue

        if steps_taken % 2 == 0:
            even_visited.add((i, j))
        else:
            odd_visited.add((i, j))

        yield i, j


def part_one(infile: Path) -> int:
    grid = parse_input(infile)
    start = np.where(grid == 1)
    coord = tuple(i[0] for i in start)

    queue = deque([(coord, 0)])
    even_visited: set[Coord] = {coord}
    odd_visited: set[Coord] = set()

    while queue:
        coord, steps_taken = queue.pop()
        for new_coord in visit_neighbours(coord, grid, even_visited, odd_visited, steps_taken, 64):
            queue.appendleft((new_coord, steps_taken + 1))

    return len(even_visited)


def part_two(infile: Path) -> int:
    grid = parse_input(infile)
    traversable_squares = set(zip(*np.where(grid != ROCK)))
    deltas = ((1, 0), (-1, 0), (0, 1), (0, -1))
    adjacency = {
        (i, j): tuple((i + di, j + dj) for di, dj in deltas if (i + di, j + dj) in traversable_squares)
        for i, j in traversable_squares
    }

    tile_swap_adjacency = defaultdict(list)
    for i in range(grid.shape[0]):
        # Moving left off the grid
        tile_swap_adjacency[(i, 0)].append(((i, grid.shape[1] - 1), (0, -1)))
        # Moving right off the grid
        tile_swap_adjacency[(i, grid.shape[1] - 1)].append(((i, 0), (0, 1)))

    for j in range(grid.shape[1]):
        # Moving up off the grid
        tile_swap_adjacency[(0, j)].append(((grid.shape[0] - 1, j), (-1, 0)))
        # Moving down off the grid
        tile_swap_adjacency[(grid.shape[0] - 1, j)].append(((0, j), (1, 0)))

    current: dict[Coord, set[Tile]] = {i: set() for i in traversable_squares}
    start_square_ = np.where(grid == 1)
    start_square = start_square_[0][0], start_square_[1][0]
    current[start_square] = {(0, 0)}

    x: list[int] = []
    y: list[int] = []

    grid_width = grid.shape[0]
    target = 26501365
    rem = target % grid_width

    for i in range(1, grid_width * 3 + 2):
        new_current: dict[Coord, set[Tile]] = defaultdict(set)
        for pos, tiles in current.items():
            for adj in adjacency[pos]:
                new_current[adj] |= tiles

        for pos, tiles in current.items():
            tile_swap_adj = tile_swap_adjacency[pos]
            if not tile_swap_adj:
                continue

            for new_pos, (di, dj) in tile_swap_adj:
                updated_tiles = {(i + di, j + dj) for i, j in tiles}
                new_current[new_pos] |= updated_tiles

        current = new_current

        if (i - rem) % grid_width == 0:
            score = sum(len(v) for v in current.values())
            y.append(score)
            x.append(len(x))

    score = sum(len(v) for v in current.values())

    coefficients = np.polyfit(x, y, 2)
    result = float(np.polyval(coefficients, (target - rem) // grid_width))
    return int(round(result, 0))
