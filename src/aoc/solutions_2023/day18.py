from __future__ import annotations

from collections.abc import Callable, Iterable, Iterator
from enum import Enum
from pathlib import Path
from typing import NamedTuple

from more_itertools import pairwise


class Coord(NamedTuple):
    i: float
    j: float


class Direction(Enum):
    U = (-1, 0)
    D = (1, 0)
    L = (0, -1)
    R = (0, 1)

    @classmethod
    def from_str(cls, string: str) -> Direction:
        match string:
            case "U" | "3":
                return Direction.U
            case "D" | "1":
                return Direction.D
            case "L" | "2":
                return Direction.L
            case "R" | "0":
                return Direction.R
            case _:
                raise ValueError()


DIRECTION_PAIR_TO_OFFSET = {
    (Direction.L, Direction.D): ((-0.5, -0.5), (0.5, 0.5)),
    (Direction.U, Direction.R): ((-0.5, -0.5), (0.5, 0.5)),
    (Direction.D, Direction.L): ((-0.5, -0.5), (0.5, 0.5)),
    (Direction.R, Direction.U): ((-0.5, -0.5), (0.5, 0.5)),
    (Direction.L, Direction.U): ((-0.5, 0.5), (0.5, -0.5)),
    (Direction.D, Direction.R): ((-0.5, 0.5), (0.5, -0.5)),
    (Direction.U, Direction.L): ((-0.5, 0.5), (0.5, -0.5)),
    (Direction.R, Direction.D): ((-0.5, 0.5), (0.5, -0.5)),
}


def parse_input_part_one(infile: Path) -> Iterator[tuple[Direction, int]]:
    with infile.open() as f:
        for line in f:
            dir_str, num, _ = line.strip().split()
            yield Direction.from_str(dir_str), int(num)


def parse_input_part_two(infile: Path) -> Iterator[tuple[Direction, int]]:
    with infile.open() as f:
        for line in f:
            _, _, hex_str = line.strip().split()
            hex_str = hex_str[2:-1]
            yield Direction.from_str(hex_str[-1]), int(hex_str[:-1], base=16)


def get_line_coords(coordinate: Coord, direction: Direction, next_direction: Direction) -> tuple[Coord, ...]:
    i, j = coordinate
    return tuple(
        Coord(
            i + di,
            j + dj,
        )
        for di, dj in DIRECTION_PAIR_TO_OFFSET[(direction, next_direction)]
    )


def get_new_coord(coordinate: Coord, direction: Direction, distance: int) -> Coord:
    return Coord(
        coordinate[0] + direction.value[0] * distance,
        coordinate[1] + direction.value[1] * distance,
    )


def solve(infile: Path, parse_func: Callable[[Path], Iterable[tuple[Direction, int]]]) -> int:
    current = Coord(0, 0)
    exterior_b = []

    instructions = list(parse_func(infile))
    instructions.append(instructions[0])
    pairwise_instructions = pairwise(instructions)

    # Manually handle first iteration
    (direction, distance), (next_direction, _) = next(pairwise_instructions)
    current = get_new_coord(current, direction, distance)

    c1, c2 = get_line_coords(current, direction, next_direction)
    exterior_a = [c1]
    exterior_b = [c2]

    for (direction, distance), (next_direction, _) in pairwise_instructions:
        current = get_new_coord(current, direction, distance)
        c1, c2 = get_line_coords(current, direction, next_direction)

        if (c1.i == exterior_a[-1].i and c2.i == exterior_b[-1].i) or (
            c1.j == exterior_a[-1].j and c2.j == exterior_b[-1].j
        ):
            exterior_a.append(c1)
            exterior_b.append(c2)
        else:
            exterior_a.append(c2)
            exterior_b.append(c1)

    exterior_a.append(exterior_a[0])
    exterior_b.append(exterior_b[0])

    area_a = abs(sum(0.5 * (i1 + i2) * (j1 - j2) for (i1, j1), (i2, j2) in pairwise(exterior_a)))
    area_b = abs(sum(0.5 * (i1 + i2) * (j1 - j2) for (i1, j1), (i2, j2) in pairwise(exterior_b)))

    return int(max([area_a, area_b]))


def part_one(infile: Path) -> int:
    return solve(infile, parse_input_part_one)


def part_two(infile: Path) -> int:
    return solve(infile, parse_input_part_two)
