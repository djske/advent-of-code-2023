from collections.abc import Callable
from functools import reduce
from operator import mul
from pathlib import Path
from typing import TypeVar

T = TypeVar("T")


def parse_input(infile: Path, line_processer: Callable[[str], T]) -> tuple[T, T]:
    with infile.open() as f:
        for line in f:
            if line.startswith("Time:"):
                time_parse = line_processer(line)
            if line.startswith("Distance:"):
                distance_parse = line_processer(line)

    return time_parse, distance_parse


def split_and_convert(string: str) -> tuple[int, ...]:
    return tuple(int(i) for i in string.split(":", 1)[1].strip().split())


def join_and_convert(string: str) -> int:
    return int("".join(string.split(":", 1)[1].strip().split()))


def part_one(infile: Path) -> int:
    times, distances = parse_input(infile, split_and_convert)

    ways_to_beat_time = []

    for time, record_distance in zip(times, distances, strict=True):
        counter = 0
        for hold_t in range(time):
            distance = (time - hold_t) * hold_t

            if distance > record_distance:
                counter += 1

        ways_to_beat_time.append(counter)

    return reduce(mul, ways_to_beat_time, 1)


def part_two(infile: Path) -> int:
    time, record_distance = parse_input(infile, join_and_convert)

    counter = 0
    for hold_t in range(time):
        distance = (time - hold_t) * hold_t

        if distance > record_distance:
            counter += 1

    return counter
