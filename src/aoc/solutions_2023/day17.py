"""Solution to Day 17 of AOC 2023.

Note: I figured out a (slow) solution by myself. When reading descriptions of
other solutions, I noticed another solver had used a heap queue. I then figured
out how to integrate heapq into my solution (without looking at theirs).
"""
from __future__ import annotations

from collections import defaultdict
from collections.abc import Callable, Iterator, Mapping
from dataclasses import dataclass
from enum import Enum
from heapq import heapify, heappop, heappush
from pathlib import Path
from typing import Any

import numpy as np
import numpy.typing as npt

Coordinate = tuple[int, int]


class Direction(Enum):
    UP = (-1, 0)
    DOWN = (1, 0)
    LEFT = (0, -1)
    RIGHT = (0, 1)
    NULL = (0, 0)


DIRECTION_PERPENDICULAR_MAP: Mapping[Direction, tuple[Direction, Direction]] = {
    Direction.LEFT: (
        Direction.UP,
        Direction.DOWN,
    ),
    Direction.RIGHT: (
        Direction.UP,
        Direction.DOWN,
    ),
    Direction.UP: (
        Direction.LEFT,
        Direction.RIGHT,
    ),
    Direction.DOWN: (
        Direction.LEFT,
        Direction.RIGHT,
    ),
    Direction.NULL: (
        Direction.RIGHT,
        Direction.DOWN,
    ),
}


@dataclass(frozen=True)
class State:
    position: Coordinate
    heat_loss: int
    direction: Direction
    direction_count: int

    def __lt__(self, other: Any) -> bool:
        if not isinstance(other, State):
            err = f"cannot compare State with {type(other).__name__}"
            raise TypeError(err)
        return self.heat_loss < other.heat_loss

    def _get_next_state_in_current_direction(
        self, max_same_direction: int, grid: npt.NDArray[np.int32]
    ) -> State | None:
        if self.direction_count < max_same_direction:
            di, dj = self.direction.value
            i, j = self.position
            new_position = di + i, dj + j

            if 0 <= new_position[0] < grid.shape[0] and 0 <= new_position[1] < grid.shape[1]:
                return State(
                    new_position, self.heat_loss + grid[new_position], self.direction, self.direction_count + 1
                )

        return None

    def get_possible_next_states(self, grid: npt.NDArray[np.int32]) -> Iterator[State]:
        max_same_direction = 3

        if state := self._get_next_state_in_current_direction(max_same_direction, grid):
            yield state

        for direction in DIRECTION_PERPENDICULAR_MAP[self.direction]:
            di, dj = direction.value
            i, j = self.position
            new_position = di + i, dj + j

            if 0 <= new_position[0] < grid.shape[0] and 0 <= new_position[1] < grid.shape[1]:
                yield State(new_position, self.heat_loss + grid[new_position], direction, 1)

    def get_possible_next_ultra_states(self, grid: npt.NDArray[np.int32]) -> Iterator[State]:
        max_same_direction = 10

        if state := self._get_next_state_in_current_direction(max_same_direction, grid):
            yield state

        # If turning, have to move at least four.
        for direction in DIRECTION_PERPENDICULAR_MAP[self.direction]:
            di, dj = direction.value
            i, j = self.position
            new_position = di * 4 + i, dj * 4 + j

            if 0 <= new_position[0] < grid.shape[0] and 0 <= new_position[1] < grid.shape[1]:
                yield State(
                    new_position,
                    self.heat_loss
                    + grid[tuple(i + di * n for n in range(1, 5)), tuple(j + dj * n for n in range(1, 5))].sum(),
                    direction,
                    4,
                )


def parse_input(infile: Path) -> npt.NDArray[np.int32]:
    with infile.open() as f:
        data = [[int(i) for i in line.strip()] for line in f]
        return np.array(data)


def run_part(infile: Path, callback: Callable[[State, npt.NDArray[np.int32]], Iterator[State]]) -> int:
    grid = parse_input(infile)

    start = State((0, 0), 0, Direction.NULL, 3)

    unevaluated_states = [start]
    heapify(unevaluated_states)
    best_states: dict[Coordinate, dict[tuple[Direction, int], float]] = defaultdict(
        lambda: defaultdict(lambda: float("inf"))
    )

    while unevaluated_states:
        current = heappop(unevaluated_states)

        # No point moving from last square.
        if current.position == (grid.shape[0] - 1, grid.shape[1] - 1):
            continue
        # Check whether this is still the best way to reach this square.
        if current.heat_loss > best_states[current.position][(current.direction, current.direction_count)]:
            continue
        # Check whether this is going to be better than the last square.
        if current.heat_loss >= min(best_states[(grid.shape[0] - 1, grid.shape[1] - 1)].values(), default=float("inf")):
            continue

        # Get the new states.
        for state in callback(current, grid):
            current_best = best_states[state.position][(state.direction, state.direction_count)]
            if current_best > state.heat_loss:
                best_states[state.position][(state.direction, state.direction_count)] = state.heat_loss
                heappush(unevaluated_states, state)

    return int(min(best_states[(grid.shape[0] - 1, grid.shape[1] - 1)].values()))


def part_one(infile: Path) -> int:
    return run_part(infile, State.get_possible_next_states)


def part_two(infile: Path) -> int:
    return run_part(infile, State.get_possible_next_ultra_states)
