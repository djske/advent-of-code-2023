from collections.abc import Mapping, Sequence
from copy import deepcopy
from dataclasses import dataclass
from functools import reduce
from pathlib import Path
from typing import Final, cast

from intervaltree import Interval, IntervalTree  # type: ignore[import-untyped]
from more_itertools import chunked

HUMIDITY_LOCATION: Final[str] = "humidity_to_location"

MAPPING_ORDER: Final[Sequence[str]] = (
    "seed_to_soil",
    "soil_to_fertilizer",
    "fertilizer_to_water",
    "water_to_light",
    "light_to_temperature",
    "temperature_to_humidity",
    HUMIDITY_LOCATION,
)


@dataclass(frozen=True)
class Input:
    seeds: Sequence[int]
    seed_to_soil: Mapping[int, int]
    soil_to_fertilizer: Mapping[int, int]
    fertilizer_to_water: Mapping[int, int]
    water_to_light: Mapping[int, int]
    light_to_temperature: Mapping[int, int]
    temperature_to_humidity: Mapping[int, int]
    humidity_to_location: Mapping[int, int]


def parse_input(infile: Path) -> Input:
    current_map = ""
    tree: IntervalTree = IntervalTree()

    dct: dict[str, dict[int, int]] = {}
    seeds: list[int] = []

    with infile.open() as f:
        for line in f:
            if line.startswith("seeds:"):
                seeds = [int(i) for i in line.replace("seeds: ", "").strip().split()]
                continue

            if line.strip().endswith("map:"):
                current_map = line.strip().replace(" map:", "").replace("-", "_")
                continue

            if not line.strip() and current_map:
                dct[current_map] = tree
                tree = IntervalTree()
                continue

            if line_info := [int(i) for i in line.strip().split()]:
                dest, source, count = line_info
                tree.add(Interval(source, source + count, data={"dest": dest, "src": source}))

    dct[HUMIDITY_LOCATION] = tree

    return Input(seeds, **dct)


def get_new_input(input_tree: IntervalTree, next_tree: IntervalTree) -> IntervalTree:
    input_tree = deepcopy(input_tree)
    next_tree = deepcopy(next_tree)

    for i in input_tree:
        next_tree.slice(i.begin)
        next_tree.slice(i.end)

    for i in next_tree:
        input_tree.slice(i.begin)
        input_tree.slice(i.end)

    new_tree = IntervalTree()

    for i in input_tree:
        if overlaps := next_tree.overlap(i):
            if len(overlaps) != 1:
                err = f"Expected overlaps to be length one, got {overlaps}"
                raise AssertionError(err)
            overlap = overlaps.pop()

            interval = Interval(
                begin=i.begin - overlap.data["src"] + overlap.data["dest"],
                end=i.end - overlap.data["src"] + overlap.data["dest"],
            )
            new_tree.add(interval)

        else:
            new_tree.add(i)

    return new_tree


def part_one(infile: Path) -> int:
    parsed_inputs = parse_input(infile)
    seeds = IntervalTree()

    for start in parsed_inputs.seeds:
        seeds.add(Interval(begin=start, end=start + 1))

    locations = reduce(
        get_new_input,
        (getattr(parsed_inputs, i) for i in MAPPING_ORDER),
        seeds,
    )

    return cast(int, min(i.begin for i in locations))


def part_two(infile: Path) -> int:
    parsed_inputs = parse_input(infile)

    seeds = IntervalTree()
    for start, delta in chunked(parsed_inputs.seeds, 2):
        seeds.add(Interval(begin=start, end=start + delta))

    locations = reduce(
        get_new_input,
        (getattr(parsed_inputs, i) for i in MAPPING_ORDER),
        seeds,
    )

    return cast(int, min(i.begin for i in locations))
