from collections.abc import Iterator, Sequence
from pathlib import Path


def iter_grid(parsed_input: Sequence[str]) -> Iterator[tuple[str, set[int], set[int]]]:
    for line in parsed_input:
        card_title, numbers = line.strip().split(":")

        winning_numbers, chosen_numbers = ({int(num) for num in text.strip().split()} for text in numbers.split("|"))

        yield card_title, winning_numbers, chosen_numbers


def part_one(infile: Path) -> int:
    parsed_input = infile.read_text().strip().split("\n")

    total = 0
    for _, winning_numbers, chosen_numbers in iter_grid(parsed_input):
        intersection = winning_numbers & chosen_numbers
        if intersection:
            total += 2 ** (len(intersection) - 1)

    return total


def part_two(infile: Path) -> int:
    parsed_input = infile.read_text().strip().split("\n")

    cards = {pos: 1 for pos, _ in enumerate(parsed_input, start=1)}

    for card_title, winning_numbers, chosen_numbers in iter_grid(parsed_input):
        card_num = int(card_title.split()[1])
        intersection = winning_numbers & chosen_numbers

        for i in range(1, len(intersection) + 1):
            if (won_card := card_num + i) in cards:
                cards[won_card] += cards[card_num]

    return sum(cards.values())
