import re
from pathlib import Path

Box = dict[str, int]


def parse_infile(infile: Path) -> list[str]:
    with infile.open() as f:
        data = f.read().strip().split(",")

    return data


def hash_string(string: str) -> int:
    current_value = 0
    for char in string:
        current_value += ord(char)
        current_value *= 17
        current_value %= 256

    return current_value


def part_one(infile: Path) -> int:
    data = parse_infile(infile)
    return sum([hash_string(string) for string in data])


def part_two(infile: Path) -> int:
    operator_match = re.compile(r"(\-|\=)")
    data = parse_infile(infile)

    boxes: dict[int, Box] = {i: {} for i in range(256)}

    for instruction in data:
        label, operator, value = operator_match.split(instruction)
        box = hash_string(label)

        if operator == "-":
            boxes[box].pop(label, None)
        elif operator == "=":
            boxes[box][label] = int(value)
        else:
            err = f"Unexpected operator {operator!r}"
            raise ValueError(err)

    total = 0
    for box, lenses in boxes.items():
        for slot_number, focal_length in enumerate(lenses.values(), start=1):
            total += (box + 1) * slot_number * focal_length

    return total
