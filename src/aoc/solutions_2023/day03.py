import itertools
import operator
from collections import defaultdict
from collections.abc import Iterator, Sequence
from pathlib import Path


def get_adjacent_squares(i: int, j: int, grid: Sequence[Sequence[str]]) -> Iterator[tuple[int, int, str]]:
    i_indexes = {i - 1, i, i + 1}
    j_indexes = {j - 1, j, j + 1}

    # Remove -1 to prevent wraparound
    i_indexes.discard(-1)
    j_indexes.discard(-1)

    to_check = set(itertools.product(i_indexes, j_indexes))
    # Remove i,j (query square)
    to_check.remove(
        (
            i,
            j,
        )
    )

    for new_i, new_j in to_check:
        try:
            char = grid[new_i][new_j]
        except IndexError:
            continue

        yield new_i, new_j, char


def is_adjacent_to_symbol(i: int, j: int, grid: Sequence[Sequence[str]]) -> bool:
    for *_, char in get_adjacent_squares(i, j, grid):
        if not char.isdigit() and not char == ".":
            return True
    return False


def get_adjacent_gears(i: int, j: int, grid: Sequence[Sequence[str]]) -> set[tuple[int, int]]:
    gears = set()
    for new_i, new_j, char in get_adjacent_squares(i, j, grid):
        if char == "*":
            gears.add(
                (
                    new_i,
                    new_j,
                )
            )

    return gears


def part_one(infile: Path) -> int:
    grid = infile.read_text().strip().split("\n")

    total = 0
    current_num = ""
    is_adj_to_symbol = False

    # Define reset closure for convenience
    def reset() -> None:
        nonlocal current_num, is_adj_to_symbol, total

        if current_num:
            if is_adj_to_symbol:
                total += int(current_num)
            current_num = ""
            is_adj_to_symbol = False

    # Main loop
    for i_idx, row in enumerate(grid):
        for j_idx, char in enumerate(row):
            if char.isdigit():
                current_num += char
                if is_adjacent_to_symbol(i_idx, j_idx, grid):
                    is_adj_to_symbol = True
            else:
                reset()
        reset()

    return total


def part_two(infile: Path) -> int:
    grid = infile.read_text().strip().split("\n")

    gears = defaultdict(list)
    current_num = ""
    adj_gears: set[tuple[int, int]] = set()

    # Define reset closure for convenience
    def reset() -> None:
        nonlocal current_num, adj_gears, gears

        if current_num:
            for gear in adj_gears:
                gears[gear].append(int(current_num))

            current_num = ""
            adj_gears = set()

    # Main loop
    for i_idx, row in enumerate(grid):
        for j_idx, j in enumerate(row):
            if j.isdigit():
                current_num += j
                adj_gears |= get_adjacent_gears(i_idx, j_idx, grid)
            else:
                reset()
        reset()

    numbers_required_adjacent_to_gear = 2
    return sum(operator.mul(*value) for value in gears.values() if len(value) == numbers_required_adjacent_to_gear)
