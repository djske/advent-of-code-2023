from itertools import combinations
from pathlib import Path

import numpy as np
import numpy.typing as npt


def parse_input(infile: Path) -> npt.NDArray[np.int32]:
    data: list[list[int]] = []
    with infile.open() as f:
        for line in f:
            data.append([0 if i != "." else 1 for i in line.strip()])

    return np.array(data, dtype=np.int64)


def set_traversal_costs(data: npt.NDArray[np.int32], expansion_factor: int) -> None:
    """Update an array so that each point represents the cost of traversing."""
    expand_cols = []
    expand_rows = []

    for idx, row in enumerate(data):
        if (row == 1).all():
            expand_rows.append(idx)

    for idx, col in enumerate(data.T):
        if (col == 1).all():
            expand_cols.append(idx)

    for col in expand_cols:
        data[:, col] = expansion_factor
    for row in expand_rows:
        data[row] = expansion_factor

    data[np.where(data == 0)] = 1


def get_manhattan_distance_between_planet_pairs(array: npt.NDArray[np.int32], expansion_factor: int) -> int:
    non_zero = np.where(array == 0)
    coords = tuple(zip(*non_zero))

    set_traversal_costs(array, expansion_factor)

    min_dist = 0

    for (ai, aj), (bi, bj) in combinations(coords, r=2):
        ai_, bi_ = sorted([ai, bi])
        aj_, bj_ = sorted([aj, bj])

        min_dist += array[ai_:bi_, aj_].sum() + array[bi_, aj_:bj_].sum()

    return min_dist


def part_one(infile: Path) -> int:
    input_arr = parse_input(infile)
    return get_manhattan_distance_between_planet_pairs(input_arr, 2)


def part_two(infile: Path) -> int:
    input_arr = parse_input(infile)
    return get_manhattan_distance_between_planet_pairs(input_arr, 1_000_000)
