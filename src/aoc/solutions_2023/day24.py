from collections.abc import Iterator
from itertools import combinations
from pathlib import Path

import numpy as np
from sympy import Symbol, solve, symbols


def parse_infile(infile: Path) -> Iterator[tuple[int, int, int, int, int, int]]:
    with infile.open() as f:
        for line in f:
            px, py, pz, vx, vy, vz = (int(i) for i in line.replace(",", "").replace("@", "").split())
            yield px, py, pz, vx, vy, vz


def part_one(infile: Path) -> int:
    vals = list(parse_infile(infile))
    counter = 0

    min_xy = 200000000000000
    max_xy = 400000000000000

    for i, j in combinations(vals, 2):
        a = np.array(
            [
                [j[3], -i[3]],
                [j[4], -i[4]],
            ]
        )

        b = np.array([i[0] - j[0], i[1] - j[1]])

        try:
            x1, x0 = np.linalg.solve(a, b)
        except np.linalg.LinAlgError:
            continue

        if x1 < 0 or x0 < 0:
            continue

        x = i[0] + i[3] * x0
        y = i[1] + i[4] * x0

        if not (min_xy <= x <= max_xy and min_xy <= y <= max_xy):
            continue
        counter += 1

    return counter


def part_two(infile: Path) -> int:
    x0_mega, y0_mega, z0_mega, vx_mega, vy_mega, vz_mega = symbols("x0 y0 z0 vx vy vz")
    counter = 0

    eqs = []
    to_make_eqs_from = []

    data = list(parse_infile(infile))
    for idx in range(3):
        data.sort(key=lambda i: i[idx])
        to_make_eqs_from.append(data[0])
        to_make_eqs_from.append(data[-1])
        to_make_eqs_from.append(data[len(data) // 2])

    for px, py, pz, vx, vy, vz in to_make_eqs_from:
        t = symbols(f"t{counter}")
        eqs.append((x0_mega + t * vx_mega) - (px + t * vx))
        eqs.append((y0_mega + t * vy_mega) - (py + t * vy))
        eqs.append((z0_mega + t * vz_mega) - (pz + t * vz))
        counter += 1

    result: dict[Symbol, int] = solve(eqs)[0]  # type: ignore[no-untyped-call]
    return result[x0_mega] + result[y0_mega] + result[z0_mega]
