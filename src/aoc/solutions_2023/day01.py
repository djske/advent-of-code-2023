from collections.abc import Iterator, Mapping
from pathlib import Path
from typing import Final

TEXT_TO_DIGIT_MAP: Final[Mapping[str, str]] = {
    "one": "1",
    "two": "2",
    "three": "3",
    "four": "4",
    "five": "5",
    "six": "6",
    "seven": "7",
    "eight": "8",
    "nine": "9",
}


def iter_input(infile: Path) -> Iterator[str]:
    with infile.open() as f:
        for line in f:
            if stripped_line := line.strip():
                yield stripped_line


def part_one(input_file: Path) -> int:
    calibration_sum = 0

    for line in iter_input(input_file):
        digits = [char for char in line if char.isdigit()]
        calibration_sum += int(f"{digits[0]}{digits[-1]}")

    return calibration_sum


def part_two(input_file: Path) -> int:
    calibration_sum = 0

    for line in iter_input(input_file):
        digits = []
        for idx, char in enumerate(line):
            if char.isdigit():
                digits.append(char)
                continue

            substr = line[idx:]
            for key, value in TEXT_TO_DIGIT_MAP.items():
                if substr.startswith(key):
                    digits.append(value)

        calibration_sum += int(f"{digits[0]}{digits[-1]}")

    return calibration_sum
