from pathlib import Path

import numpy as np
import numpy.typing as npt


def parse_input(infile: Path) -> npt.NDArray[np.int64]:
    data = []

    char_map = {".": 0, "O": 1, "#": 2}

    with infile.open() as f:
        for line in f:
            row = [char_map[char] for char in line.strip()]
            data.append(row)

    return np.array(data, dtype=np.int64)


def tilt(grid: npt.NDArray[np.int64], direction: str) -> npt.NDArray[np.int64]:
    match direction:
        case "N":
            pass
        case "S":
            grid = np.rot90(grid, k=2)
        case "E":
            grid = np.rot90(grid, k=1)
        case "W":
            grid = np.rot90(grid, k=-1)

    # Rounded rocks
    for i_, j in zip(*np.where(grid == 1)):
        i = i_
        while i != 0 and grid[i - 1, j] == 0:
            grid[i - 1, j] = 1
            grid[i, j] = 0
            i -= 1

    match direction:
        case "N":
            pass
        case "S":
            grid = np.rot90(grid, k=2)
        case "E":
            grid = np.rot90(grid, k=-1)
        case "W":
            grid = np.rot90(grid, k=1)

    return grid


def calculte_load(grid: npt.NDArray[np.int64]) -> int:
    n_rows = len(grid)

    total = 0
    for idx, row in enumerate(grid):
        total += (n_rows - idx) * sum(1 for i in row if i == 1)

    return total


def part_one(infile: Path) -> int:
    data = parse_input(infile)
    data = tilt(data, "N")
    return calculte_load(data)


def to_state(grid: npt.NDArray[np.int64]) -> tuple[int, ...]:
    return tuple(sum(3**idx * int(i) for idx, i in enumerate(row)) for row in grid)


def part_two(infile: Path) -> int:
    states = []
    seen = set()

    data = parse_input(infile)
    state = (to_state(data), calculte_load(data))
    states.append(state)
    seen.add(state)

    while True:
        data = tilt(data, "N")
        data = tilt(data, "W")
        data = tilt(data, "S")
        data = tilt(data, "E")

        state = (to_state(data), calculte_load(data))
        states.append(state)
        if state in seen:
            break
        seen.add(state)

    cycle_start = states.index(states[-1])
    cycle_length = len(states) - states.index(states[-1]) - 1

    finish_delta = (1_000_000_000 - cycle_start) % cycle_length
    return states[cycle_start + finish_delta][1]
