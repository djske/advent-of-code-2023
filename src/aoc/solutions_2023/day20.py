import math
from collections import deque
from collections.abc import Iterator
from dataclasses import dataclass
from enum import Enum
from pathlib import Path
from typing import cast


class Signal(Enum):
    HIGH = "High"
    LOW = "Low"


SignalTuple = tuple[str, Signal, str]


@dataclass
class Module:
    name: str
    outputs: tuple[str, ...]

    def send(self, _signal: Signal, _source: str) -> Iterator[SignalTuple]:
        yield from ()


@dataclass
class FlipFlipModule(Module):
    on: bool = False

    def send(self, signal: Signal, _: str) -> Iterator[SignalTuple]:
        if signal == Signal.HIGH:
            return
        self.on ^= True

        return_signal = Signal.HIGH if self.on else Signal.LOW
        for i in self.outputs:
            yield (i, return_signal, self.name)


@dataclass
class ConjunctionModule(Module):
    inputs: dict[str, Signal]

    def send(self, signal: Signal, source: str) -> Iterator[SignalTuple]:
        self.inputs[source] = signal
        return_signal = Signal.HIGH
        if all(value == Signal.HIGH for value in self.inputs.values()):
            return_signal = Signal.LOW

        for i in self.outputs:
            yield (i, return_signal, self.name)


@dataclass
class BroadcastModule(Module):
    def send(self, signal: Signal, _: str) -> Iterator[SignalTuple]:
        for i in self.outputs:
            yield (i, signal, self.name)


@dataclass
class Modules:
    _modules: dict[str, Module]

    def __getitem__(self, value: str) -> Module:
        return self._modules[value]


def parse_input(infile: Path) -> Modules:
    module_dict: dict[str, Module] = {}

    with infile.open() as f:
        for line in f:
            module_str, output_str = (i.strip() for i in line.split("->"))
            outputs = tuple(i.strip() for i in output_str.split(","))

            module: Module
            if module_str == "broadcaster":
                module = BroadcastModule(module_str, outputs)
            elif module_str.startswith("%"):
                module = FlipFlipModule(module_str[1:], outputs)
            elif module_str.startswith("&"):
                module = ConjunctionModule(module_str[1:], outputs, {})
            else:
                err = f"unexpected start character {module_str[0]!r}"
                raise ValueError(err)

            module_dict[module.name] = module

    module_dict["rx"] = Module("rx", ())

    for module in module_dict.values():
        for output in module.outputs:
            output_mod = module_dict[output]
            if isinstance(output_mod, ConjunctionModule):
                output_mod.inputs[module.name] = Signal.LOW

    return Modules(module_dict)


def part_one(infile: Path) -> int:
    modules = parse_input(infile)
    counts = {Signal.HIGH: 0, Signal.LOW: 0}
    queue: deque[SignalTuple] = deque()

    for _ in range(1000):
        queue.appendleft(("broadcaster", Signal.LOW, ""))

        while queue:
            destination, signal, source = queue.pop()

            counts[signal] += 1
            for i in modules[destination].send(signal, source):
                queue.appendleft(i)

    return counts[Signal.HIGH] * counts[Signal.LOW]


def part_two(infile: Path) -> int:
    modules = parse_input(infile)
    queue: deque[SignalTuple] = deque()

    # Assumption: The source modules to the conjunction module before rx (k1)
    # send a high signal every N presses. The result will be the LCM of these
    # values.
    cycle_period = {input_mod: 0 for input_mod in cast(ConjunctionModule, modules["kl"]).inputs}
    counter = 0

    def push_button() -> None:
        queue.appendleft(("broadcaster", Signal.LOW, ""))

        while queue:
            destination, signal, source = queue.pop()
            if destination == "kl" and signal == Signal.HIGH:
                if not cycle_period.get(source):
                    cycle_period[source] = counter

            for i in modules[destination].send(signal, source):
                queue.appendleft(i)

    while True:
        counter += 1
        push_button()
        if all(cycle_period.values()):
            break

    return math.lcm(*cycle_period.values())
