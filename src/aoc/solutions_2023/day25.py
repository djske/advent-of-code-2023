from itertools import combinations
from math import comb
from pathlib import Path

import networkx as nx  # type: ignore[import-untyped]
from tqdm import tqdm


def parse_infile(infile: Path) -> nx.Graph:
    g = nx.Graph()
    with infile.open() as f:
        for line in f:
            source, dests = line.strip().split(": ")
            for dest in dests.split(" "):
                g.add_edge(source, dest)

    return g


def part_one(infile: Path) -> int:
    target_n_components = 2

    graph = parse_infile(infile)

    # Nodes between the two groups are likely to have a higher betweenness centrality.
    centrality = nx.betweenness_centrality(graph, k=100)
    candidates = sorted(centrality, key=lambda i: centrality[i], reverse=True)[:10]
    candidate_edges = tuple((i, j) for i, j in combinations(candidates, 2))

    result = -1

    for edge_set in tqdm(combinations(candidate_edges, 3), total=comb(len(candidate_edges), 3)):
        graph_cp = graph.copy()
        graph_cp.remove_edges_from(edge_set)
        components = list(nx.connected_components(graph_cp))
        if len(components) == target_n_components:
            result = len(components[0]) * len(components[1])
            break

    return result
