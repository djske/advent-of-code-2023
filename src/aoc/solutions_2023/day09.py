from functools import reduce
from pathlib import Path

from more_itertools import pairwise


def parse_input(
    infile: Path,
) -> list[list[int]]:
    with infile.open() as f:
        return [[int(i) for i in line.strip().split()] for line in f]


def construct_deltas(row: list[int]) -> list[list[int]]:
    """Calculate the list of deltas.

    An example input and output is given below.

    Input:
    [1, 2, 4, 7, 11]

    Output:
    [
        [1, 2, 4, 7, 11],
        [1, 2, 3, 4],
        [1, 1, 1],
        [0, 0],
    ]
    """
    deltas = [row]

    while True:
        delta = [j - i for i, j in pairwise(deltas[-1])]
        deltas.append(delta)
        if all(i == 0 for i in delta):
            break

    return deltas


def predict_next(row: list[int]) -> int:
    deltas = construct_deltas(row)
    return sum(i[-1] for i in deltas)


def predict_previous(row: list[int]) -> int:
    deltas = construct_deltas(row)
    return reduce(lambda i, j: j - i, [i[0] for i in reversed(deltas)])


def part_one(infile: Path) -> int:
    value_history = parse_input(infile)
    return sum(predict_next(history) for history in value_history)


def part_two(infile: Path) -> int:
    value_history = parse_input(infile)
    return sum(predict_previous(history) for history in value_history)
