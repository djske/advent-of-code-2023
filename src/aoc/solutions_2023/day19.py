import operator
import re
from collections import deque
from collections.abc import Iterator
from copy import deepcopy
from dataclasses import dataclass
from functools import reduce
from pathlib import Path

from intervaltree import Interval, IntervalTree  # type: ignore[import-untyped]

Part = dict[str, int]
State = dict[str, IntervalTree]

WORKFLOW_SPLIT_RE = re.compile(r"\{|,|\}")
RULE_SPLIT_RE = re.compile(r"(>|<|:)")


def parse_input(infile: Path) -> tuple["Workflows", list[Part]]:
    workflows = []
    parts = []

    with infile.open() as f:
        while line := next(f).strip():
            wf, *rules, fallthrough = WORKFLOW_SPLIT_RE.split(line)[:-1]
            rule_lst = [Rule.from_string(rule) for rule in rules]
            workflows.append(Workflow(wf, rule_lst, fallthrough))

        while line := next(f, "").strip():
            part = {k: int(v) for k, v in [prop.split("=") for prop in line[1:-1].split(",")]}
            parts.append(part)

    return Workflows.from_workflow_list(workflows), parts


@dataclass
class Rule:
    symbol: str
    op_str: str
    value: int
    next_wf: str

    @property
    def tree(self) -> IntervalTree:
        if self.op_str == ">":
            return IntervalTree([Interval(self.value + 1, 4_001)])
        else:
            return IntervalTree([Interval(1, self.value)])

    @classmethod
    def from_string(cls, string: str) -> "Rule":
        prop, op_str, value, _, workflow_if_true = RULE_SPLIT_RE.split(string)
        return Rule(prop, op_str, int(value), workflow_if_true)

    def evaluate(self, part: Part) -> str | None:
        if self.op_str == ">" and part[self.symbol] > self.value:
            return self.next_wf
        elif self.op_str == "<" and part[self.symbol] < self.value:
            return self.next_wf
        return None

    def apply_to_range(self, state: State) -> Iterator[tuple[State, str | None]]:
        inc_tree = deepcopy(state[self.symbol])
        self_tree = self.tree

        for i in inc_tree:
            self_tree.slice(i.begin)
            self_tree.slice(i.end)

        for i in self_tree:
            inc_tree.slice(i.begin)
            inc_tree.slice(i.end)

        for i in inc_tree:
            # Overlaps means we map to new workflow
            if self_tree.overlap(i):
                new_state = deepcopy(state)
                new_state[self.symbol] = IntervalTree([i])
                yield new_state, self.next_wf

            # Otherwise we map to None (the workflow's fallthrough workflow)
            else:
                new_state = deepcopy(state)
                new_state[self.symbol] = IntervalTree([i])
                yield new_state, None


@dataclass
class Workflow:
    name: str
    rules: list[Rule]
    fallthrough: str

    def run(self, part: Part) -> str:
        for rule in self.rules:
            if next_wf := rule.evaluate(part):
                return next_wf
        return self.fallthrough


@dataclass
class Workflows:
    workflows: dict[str, Workflow]

    def __getitem__(self, value: str) -> Workflow:
        return self.workflows[value]

    @classmethod
    def from_workflow_list(cls, lst: list[Workflow]) -> "Workflows":
        return Workflows({wf.name: wf for wf in lst})

    def run(self, part: Part) -> bool:
        workflow = self.workflows["in"]

        while True:
            match workflow.run(part):
                case "A":
                    return True
                case "R":
                    return False
                case new_wf:
                    workflow = self.workflows[new_wf]


def part_one(infile: Path) -> int:
    total = 0
    workflows, parts = parse_input(infile)
    for part in parts:
        if workflows.run(part):
            total += sum(part.values())

    return total


def part_two(infile: Path) -> int:
    workflows, _ = parse_input(infile)

    state = {v: IntervalTree([Interval(1, 4001)]) for v in "xmas"}
    total = 0

    queue: deque[tuple[str, State]] = deque([("in", state)])
    while queue:
        wf_name, state = queue.pop()
        workflow = workflows[wf_name]

        for rule in workflow.rules:
            for next_state, next_wf in rule.apply_to_range(state):
                if next_wf is None:
                    state = next_state
                elif next_wf == "R":
                    continue
                elif next_wf == "A":
                    total += reduce(operator.mul, (i.end() - i.begin() for i in next_state.values()))
                else:
                    tup = (next_wf, next_state)
                    queue.appendleft(tup)

        if workflow.fallthrough == "R":
            continue
        elif workflow.fallthrough == "A":
            total += reduce(operator.mul, (i.end() - i.begin() for i in state.values()))
        else:
            queue.appendleft((workflow.fallthrough, state))

    return total
