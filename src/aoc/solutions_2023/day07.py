from abc import ABC, abstractmethod
from collections import Counter
from dataclasses import dataclass
from enum import IntEnum
from pathlib import Path
from typing import Mapping, Sequence, TypeVar

T = TypeVar("T")

CARDS = "AKQJT98765432"


class HandType(IntEnum):
    FIVE_OF_A_KIND = 6
    FOUR_OF_A_KIND = 5
    FULL_HOUSE = 4
    THREE_OF_A_KIND = 3
    TWO_PAIR = 2
    ONE_PAIR = 1
    HIGH_CARD = 0


class HandEvaluator(ABC):
    @classmethod
    @abstractmethod
    def hand_type(cls, cards: str) -> HandType:
        ...

    @classmethod
    @abstractmethod
    def hand_score(cls, cards: str) -> int:
        ...

    @staticmethod
    def evaluate_hand_score(cards: str, card_score: Mapping[str, int]) -> int:
        return sum((13**idx) * card_score[card] for idx, card in enumerate(reversed(cards)))

    @staticmethod
    def evaluate_hand(cards: str) -> HandType:
        hand_counts = Counter(cards)
        count_counts = Counter(hand_counts.values())

        match tuple(count_counts.get(i) for i in range(1, 6)):
            case (_, _, _, _, 1):
                return HandType.FIVE_OF_A_KIND
            case (_, _, _, 1, _):
                return HandType.FOUR_OF_A_KIND
            case (_, 1, 1, _, _):
                return HandType.FULL_HOUSE
            case (_, _, 1, _, _):
                return HandType.THREE_OF_A_KIND
            case (_, 2, _, _, _):
                return HandType.TWO_PAIR
            case (_, 1, _, _, _):
                return HandType.ONE_PAIR
            case _:
                return HandType.HIGH_CARD

    @classmethod
    def get_hand_tuple(cls, cards: str) -> tuple[int, int]:
        return (cls.hand_type(cards), cls.hand_score(cards))


class DefaultHandEvaluator(HandEvaluator):
    DEFAULT_CARD_ORDER: Mapping[str, int] = {card: 12 - idx for idx, card in enumerate("AKQJT98765432")}

    @classmethod
    def hand_type(cls, cards: str) -> HandType:
        return cls.evaluate_hand(cards)

    @classmethod
    def hand_score(cls, cards: str) -> int:
        return cls.evaluate_hand_score(cards, cls.DEFAULT_CARD_ORDER)


class JIsJokerHandEvaluator(HandEvaluator):
    J_IS_JOKER_CARD_ORDER: Mapping[str, int] = {card: 12 - idx for idx, card in enumerate("AKQT98765432J")}
    NON_JOKER_CARDS: Sequence[str] = [card for card in CARDS if card != "J"]

    @classmethod
    def hand_type(cls, cards: str) -> HandType:
        joker_count = cards.count("J")
        non_joker_cards = "".join(card for card in cards if card != "J")

        highest_type = HandType.HIGH_CARD

        # Brute force alternatives for the joker.
        for card in cls.NON_JOKER_CARDS:
            hand = non_joker_cards + joker_count * card
            if (hand_type := cls.evaluate_hand(hand)) > highest_type:
                highest_type = hand_type
                if highest_type == HandType.FIVE_OF_A_KIND:
                    return highest_type

        return highest_type

    @classmethod
    def hand_score(cls, cards: str) -> int:
        return cls.evaluate_hand_score(cards, cls.J_IS_JOKER_CARD_ORDER)


@dataclass
class Input:
    hand: str
    bid: int

    @classmethod
    def from_string(cls, string: str) -> "Input":
        hand, bid = string.split()
        return cls(hand, int(bid))

    def get_hand_tuple(self, evaluator: type[HandEvaluator]) -> tuple[int, int]:
        return evaluator.get_hand_tuple(self.hand)


def run(infile: Path, hand_evaluator: type[HandEvaluator]) -> int:
    """Shared logic of running both parts of the day's challenge.

    The logic only differs by how the hands are evaluated, so this evaluation
    is encapsulated by a HandEvaluator class, which is passed as a parameter.
    """
    data = infile.read_text().strip()

    inputs = [Input.from_string(line) for line in data.split("\n")]
    inputs.sort(key=lambda i: i.get_hand_tuple(hand_evaluator))
    total = 0
    for idx, puzzle_input in enumerate(inputs, start=1):
        total += idx * puzzle_input.bid
    return total


def part_one(infile: Path) -> int:
    return run(infile, DefaultHandEvaluator)


def part_two(infile: Path) -> int:
    return run(infile, JIsJokerHandEvaluator)
