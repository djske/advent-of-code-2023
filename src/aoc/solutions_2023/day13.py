from collections.abc import Iterator
from pathlib import Path

import numpy as np
import numpy.typing as npt


def parse_input(infile: Path) -> Iterator[npt.NDArray[np.int8]]:
    data: list[list[int]] = []

    with infile.open() as f:
        for line in f:
            stripped_line = line.strip()
            if not stripped_line and data:
                yield np.array(data, dtype=np.int8)
                data = []

            elif stripped_line:
                data.append([1 if i == "#" else 0 for i in line.strip()])

    yield np.array(data, dtype=np.int8)


def find_relection_idx(arr: npt.NDArray[np.int8]) -> int:
    for idx in range(1, len(arr)):
        length = min(idx, len(arr) - idx)
        if ((arr[idx - length : idx] - arr[idx : idx + length][::-1]) == 0).all():
            return idx

    return 0


def find_smudged_reflection_idx(arr: npt.NDArray[np.int8]) -> int:
    for idx in range(1, len(arr)):
        length = min(idx, len(arr) - idx)

        diff = arr[idx - length : idx] - arr[idx : idx + length][::-1]
        if np.absolute(diff).sum() == 1:
            return idx

    return 0


def part_one(infile: Path) -> int:
    total = 0
    for arr in parse_input(infile):
        total += find_relection_idx(arr) * 100
        total += find_relection_idx(arr.T)

    return total


def part_two(infile: Path) -> int:
    total = 0
    for arr in parse_input(infile):
        total += find_smudged_reflection_idx(arr) * 100
        total += find_smudged_reflection_idx(arr.T)

    return total
