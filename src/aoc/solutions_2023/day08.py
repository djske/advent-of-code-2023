import math
from functools import reduce
from itertools import cycle
from pathlib import Path


def parse_input(infile: Path) -> tuple[str, dict[str, dict[str, str]]]:
    nodes: dict[str, dict[str, str]] = {}
    path = ""

    with infile.open() as f:
        for line in f:
            stripped_line = line.strip()

            if not stripped_line:
                continue

            if "=" in stripped_line:
                key, next_nodes = line.split("=")
                left, right = next_nodes.strip()[1:-1].split(",")

                nodes[key.strip()] = {"L": left.strip(), "R": right.strip()}

            else:
                path = line.strip()

    return path, nodes


def part_one(infile: Path) -> int:
    path, nodes = parse_input(infile)

    current = "AAA"
    result = -1

    for step, direction in enumerate(cycle(path), start=1):
        current = nodes[current][direction]

        if current == "ZZZ":
            result = step
            break

    return result


def is_periodic_with_only_z_at_cycle_length(start: str, path: str, nodes: dict[str, dict[str, str]]) -> int:
    seen = set()
    history = []

    current = start

    for idx, direction in cycle(enumerate(path)):
        tup = (current, idx)
        history.append(tup)

        if tup in seen:
            break

        seen.add(tup)

        current = nodes[current][direction]

    cycle_start = history.index(history[-1])
    cycle_length = len(history) - cycle_start - 1

    # Doing -1, as the node ending with Z could be the first in the cycle.
    endswith_z_positions = [(pos, node) for pos, (node, _) in enumerate(history[:-1]) if node.endswith("Z")]

    # Not only one Z
    if len(endswith_z_positions) != 1:
        return -1

    # Occurrence is not at cycle length
    pos, _ = endswith_z_positions.pop()
    if pos != cycle_length:
        return -1

    # Z does not occur within the cycle
    if pos < cycle_start:
        return -1

    return pos


def part_two(infile: Path) -> int:
    path, nodes = parse_input(infile)
    start_nodes = [node for node in nodes if node.endswith("A")]
    periods = [is_periodic_with_only_z_at_cycle_length(start, path, nodes) for start in start_nodes]

    if all(i != -1 for i in periods):
        return reduce(math.lcm, periods)

    current_nodes = start_nodes

    result = -1

    for step, direction in enumerate(cycle(path), start=1):
        current_nodes = [nodes[node][direction] for node in current_nodes]

        if all(node.endswith("Z") for node in current_nodes):
            result = step
            break

    return result
