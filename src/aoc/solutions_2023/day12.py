"""Solution to Day 12 of AOC 2023.

Note: I couldn't figure out the solution after a few hours of thinking, so
went to the AOC subreddit. I didn't read any descriptions of solutions, but
did look at this diagram to inspire this solution:

- https://www.reddit.com/media?url=https%3A%2F%2Fi.redd.it%2Fl509p5z8uv5c1.png
"""

from collections import defaultdict
from dataclasses import dataclass
from pathlib import Path


@dataclass(frozen=True)
class State:
    group: int = 0
    amount: int = 0

    def finish_group(self) -> "State":
        return State(self.group + 1, 0)

    def increment_amount(self) -> "State":
        return State(self.group, self.amount + 1)


@dataclass
class HotSpringRowRecord:
    data: str
    damaged_groups: list[int]


def update_for_damaged_node(permutation_states: dict[State, int], record: HotSpringRowRecord) -> dict[State, int]:
    new_permutation_states: dict[State, int] = defaultdict(int)

    for state, permutations in permutation_states.items():
        new_state = state.increment_amount()

        if len(record.damaged_groups) == new_state.group:
            continue
        if record.damaged_groups[new_state.group] >= state.amount:
            new_permutation_states[new_state] = permutations

    return new_permutation_states


def update_for_normal_node(permutation_states: dict[State, int], record: HotSpringRowRecord) -> dict[State, int]:
    new_permutation_states: dict[State, int] = defaultdict(int)

    for state, permutations in permutation_states.items():
        # If we've been counting damaged nodes, we have to finish.
        if state.amount:
            if len(record.damaged_groups) == state.group:
                continue
            # If we've not reached the amount, this is not a valid path.
            if record.damaged_groups[state.group] != state.amount:
                continue
            # Finish group.
            new_state = state.finish_group()
            new_permutation_states[new_state] += permutations

        else:
            new_permutation_states[state] += permutations

    return new_permutation_states


def update_for_unknown_node(permutation_states: dict[State, int], record: HotSpringRowRecord) -> dict[State, int]:
    new_permutation_states: dict[State, int] = defaultdict(int)

    for state, permutations in permutation_states.items():
        # If we've been counting damaged nodes, we either have to finish or
        # increment.
        if state.amount:
            if len(record.damaged_groups) == state.group:
                continue

            # Too low, we have to increment.
            if record.damaged_groups[state.group] > state.amount:
                new_state = state.increment_amount()
                new_permutation_states[new_state] += permutations

            # Reached total, had to finish.
            elif record.damaged_groups[state.group] == state.amount:
                new_state = state.finish_group()
                new_permutation_states[new_state] += permutations

        # Otherwise, we can try both.
        else:
            # Another normal spring
            new_permutation_states[state] += permutations
            # Reaching the first damaged spring
            new_state = state.increment_amount()
            new_permutation_states[new_state] += permutations

    return new_permutation_states


def solve(record: HotSpringRowRecord) -> int:
    permutation_states = {State(): 1}

    for char in record.data:
        if char == "#":
            permutation_states = update_for_damaged_node(permutation_states, record)
        elif char == ".":
            permutation_states = update_for_normal_node(permutation_states, record)
        elif char == "?":
            permutation_states = update_for_unknown_node(permutation_states, record)

    end_states = [
        State(len(record.damaged_groups), 0),
        State(len(record.damaged_groups) - 1, record.damaged_groups[-1]),
    ]
    return sum(permutation_states[state] for state in end_states)


def parse_input(infile: Path) -> list[HotSpringRowRecord]:
    records = []

    with infile.open() as f:
        for line in f:
            data, damaged = line.strip().split()
            damaged_groups = [int(i) for i in damaged.split(",")]
            records.append(HotSpringRowRecord(data, damaged_groups))

    return records


def part_one(infile: Path) -> int:
    total = 0
    for i in parse_input(infile):
        total += solve(i)
    return total


def unfold_record(record: HotSpringRowRecord) -> HotSpringRowRecord:
    new_data = "?".join(record.data for _ in range(5))
    new_damaged_groups = record.damaged_groups * 5
    return HotSpringRowRecord(new_data, new_damaged_groups)


def part_two(infile: Path) -> int:
    total = 0
    data = parse_input(infile)
    tripled_data = [unfold_record(i) for i in data]
    for i in tripled_data:
        total += solve(i)
    return total
